Feature: Login to leaftap application & create Lead
Scenario Outline: Create Lead
Given Enter the username as DemoSalesManager
And Enter the password as crmsfa
And click on the login button
And click CRMSFA link
And click leads
And click Create lead
And Enter Company name as <Company Name>
And Enter First name as <First Name>
And Enter last name as <Last Name>
When click on the create lead button
Then verify firstname is same as entered <First Name>
Examples:
|Company Name|First Name|Last Name|
|Accenture|Prabha|Karan|
|TCS|Sathya|Narayanan|
