Feature: Login to leaftap application & create Lead
Scenario: Create Lead
Given open the browser
And maximize the browser
And Load the URL
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And click on the login button
And click CRMSFA link
And click leads
And click Create lead
And Enter Company name as Accenture
And Enter First name as Prabhakaran
And Enter last name as Nageswaran
When click on the create lead button
Then verify firstname is same as entered