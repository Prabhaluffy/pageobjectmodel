Feature: Login to leaftap application & create Lead
Background:
Given open the browser
And maximize the browser
And Load the URL
@smoke
Scenario Outline: Create Lead
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And click on the login button
And click CRMSFA link
And click leads
And click Create lead
And Enter Company name as <Company Name>
And Enter First name as <First Name>
And Enter last name as <Last Name>
When click on the create lead button
Then verify firstname is same as entered
Examples:
|Company Name|First Name|Last Name|
|Accenture|Prabha|Karan|
|TCS|Sathya|Sathya|

@reg
Scenario: Neagtive Create Lead
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And click on the login button
And click CRMSFA link
And click leads
And click Create lead
And Enter Company name as Accenture
And Enter First name as Prabhakaran
And Enter last name as Nageswaran
When click on the create lead button
But unable to create lead