package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.projectwrapper.Annotations;

public class TC001_LoginAndLogout extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName= "TC001_LoginAndLogout";
		testcaseDec = "Login into LeafTaps";
		author      = "Prabhakaran";
		category    = "Smoke";
		excelfilename = "TC01";
	}
	@Test(dataProvider="fetchdata")
	public void LoginAndLogout(String uname,String pwd) {
		new LoginPage()
		.enterUserName(uname)
		.enterPassWord(pwd)
		.clickLogin()
		.clickLogout();
	}

}
