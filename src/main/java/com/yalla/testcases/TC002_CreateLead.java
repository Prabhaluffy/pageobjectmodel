package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.projectwrapper.Annotations;

public class TC002_CreateLead extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName= "TC002_CreateLead";
		testcaseDec = "Create Lead";
		author      = "Prabhakaran";
		category    = "Smoke";
		excelfilename = "TC02";
	}
	@Test(dataProvider="fetchdata")
	public void LoginAndLogout(String uname,String pwd,String cname,String fname,String lname) {
		new LoginPage()
		.enterUserName(uname)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickcreateLeads()
		.enterCompanyName(cname)
		.enterfirstName(fname)
		.enterlastName(lname)
		.clickcreatelead()
		.verifyfirstname(fname);
		}

}
