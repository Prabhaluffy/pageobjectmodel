package com.yalla.projectwrapper;

import com.yalla.selenium.api.base.SeleniumBase;
import com.yalla.utils.Readexcel;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import org.testng.annotations.AfterMethod;

public class Annotations extends SeleniumBase{
	
	@DataProvider(name="fetchdata")
	public Object[][] fetchdata() throws IOException {
		return Readexcel.readexcelfile(excelfilename);
	}
	
  @BeforeMethod
  public void beforeMethod() {
	  	startApp("chrome", "http://leaftaps.com/opentaps/");
		//clearAndType(locateElement("id", "username"), username);
		//clearAndType(locateElement("id", "password"), "crmfsa");
		//click(locateElement("class", "decorativeSubmit"));
  }

  @AfterMethod
  public void afterMethod() {
	  close();
  }

}
