package com.yalla.utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Readexcel {
	public static Object[][] readexcelfile(String excelfilename) throws IOException {
		XSSFWorkbook wbook=new XSSFWorkbook("./Testdata/"+excelfilename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row Count is : "+rowcount);
		short colcount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count is : "+colcount);
		Object[][] data=new Object[rowcount][colcount];
		for(int i=1;i<= rowcount;i++) {
			XSSFRow row = sheet.getRow(i);
			for(int j=0;j<colcount;j++) {
				XSSFCell cell = row.getCell(j);
				String value = cell.getStringCellValue();
				data[i-1][j]=value;
				System.out.println(value);
			}
		}
		wbook.close();
		return data;
	}
}
