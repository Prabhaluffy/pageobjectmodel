package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class LoginPage extends Annotations{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.ID,using="username") WebElement eleUserName;
	@FindBy(how=How.ID,using="password") WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	@Given("Enter the username as (.*)")
	public LoginPage enterUserName(String username) {
		clearAndType(eleUserName,username);
		return this;
	}
	@And("Enter the password as (.*)")
	public LoginPage enterPassWord(String password) {
		clearAndType(elePassWord, password);
		return this;
	}
	@And("click on the login button")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	

}
