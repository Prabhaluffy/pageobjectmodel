package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.And;

public class MyHome extends Annotations {
	public MyHome() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.LINK_TEXT,using="Leads") WebElement eleClickLeads;
	@And("click leads")
	public MyLeads clickLeads() {
		click(eleClickLeads);
		return new MyLeads();
	}
}
