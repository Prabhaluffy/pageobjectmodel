package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLead extends Annotations {
	public CreateLead() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement elefirstname;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement elelastname;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleclickcreateLead;
	@And("Enter Company name as (.*)")
	public CreateLead enterCompanyName(String Companyname) {
		clearAndType(eleCompanyName,Companyname);
		return this;
	}
	@And("Enter First name as (.*)")
	public CreateLead enterfirstName(String firstname) {
		clearAndType(elefirstname,firstname);
		return this;
	}
	@And("Enter last name as (.*)")
	public CreateLead enterlastName(String lastname) {
		clearAndType(elelastname,lastname);
		return this;
	}
	@When("click on the create lead button")
	public ViewLead clickcreatelead() {
		click(eleclickcreateLead);
		return new ViewLead();
	}
	
}
