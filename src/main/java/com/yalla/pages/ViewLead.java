package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.Then;

public class ViewLead extends Annotations {
	public ViewLead() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleverifyfname;
	@Then("verify firstname is same as entered (.*)")
	public ViewLead verifyfirstname(String fname) {
		 verifyExactText(eleverifyfname, fname);
		return this;
	}	
}
