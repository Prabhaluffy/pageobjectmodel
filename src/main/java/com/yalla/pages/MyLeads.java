package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.And;

public class MyLeads extends Annotations {
	public MyLeads() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleClickCreateLead;
	@And("click Create lead")
	public CreateLead clickcreateLeads() {
		click(eleClickCreateLead);
		return new CreateLead();
	}
}
