package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.projectwrapper.Annotations;

import cucumber.api.java.en.And;

public class HomePage extends Annotations{
	
	public HomePage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogout;
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA") WebElement eleCRMSFA;
	public HomePage clickLogout() {
		click(eleLogout);
		return this;
	}
	@And("click CRMSFA link")
	public MyHome clickCRM() {
		click(eleCRMSFA);
		return new MyHome();
	}
}
