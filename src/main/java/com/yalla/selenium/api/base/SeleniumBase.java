package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;
import com.yalla.utils.Reports;

public class SeleniumBase extends Reports implements Browser, Element{

	public static RemoteWebDriver driver;
	public static WebDriverWait wait;
	int i = 1;
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked", "pass"); 
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" could not be clicked", "fail");
			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		} finally { 
			takeSnap();
		}
	}

	public void clickWithNoSnap(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked", "pass");
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" could not be clicked", "fail");
			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		}

	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.clear();
			reportStep("The field is cleared Successfully", "pass");
			//System.out.println("The field is cleared Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The field is not Interactable", "fail");
			//System.err.println("The field is not Interactable");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :"+data+" entered Successfully", "pass");
			//System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element "+ele+" is not Interactable", "fail");
			//System.err.println("The Element "+ele+" is not Interactable");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		return text;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		String cssValue = ele.getCssValue("color");
		return cssValue;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		String attributeValue = ele.getAttribute("value");
		return attributeValue;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select dropDown=new Select(ele);
		dropDown.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		Select dropDown=new Select(ele);
		dropDown.selectByIndex(index);

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select dropDown=new Select(ele);
		dropDown.selectByValue(value);
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			if(ele.getText().equals(expectedText)) {
				reportStep("The expected text contains the actual "+expectedText, "pass");
				//System.out.println("The expected text contains the actual "+expectedText);
				return true;
			}else {
				reportStep("The expected text doesn't contains the actual "+expectedText, "fail");
				//System.out.println("The expected text doesn't contain the actual "+expectedText);
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "fail");
			//System.out.println("Unknown exception occured while verifying the Text");
		} 

		return false;
	}

	@Override
	public boolean verifyPartialText(String string, String expectedText) {
		// TODO Auto-generated method stub
		try {
			if(string.contains(expectedText)) {
				reportStep("The expected text contains the actual "+expectedText, "pass");
				//System.out.println("The expected text contains the actual "+expectedText);
				return true;
			}else {
				reportStep("The expected text doesn't contains the actual "+expectedText, "fail");
				//System.out.println("The expected text doesn't contain the actual "+expectedText);
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Text", "fail");
			//System.out.println("Unknown exception occured while verifying the Text");
		} 

		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			if(ele.getAttribute(attribute).equals(value)) {
				reportStep("The expected attribute :"+attribute+" value contains the actual "+value, "pass");
				//System.out.println("The expected attribute :"+attribute+" value contains the actual "+value);
				return true;
			}else {
				reportStep("The expected attribute :"+attribute+" value doesn't contain the actual "+value, "fail");
				//System.out.println("The expected attribute :"+attribute+" value does not contains the actual "+value);
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Attribute Text", "fail");
			//System.out.println("Unknown exception occured while verifying the Attribute Text");
		}
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			if(ele.getAttribute(attribute).contains(value)) {
				reportStep("The expected attribute :"+attribute+" value contains the actual "+value, "pass");
				//System.out.println("The expected attribute :"+attribute+" value contains the actual "+value);
			}else {
				reportStep("The expected attribute :"+attribute+" value doesn't contain the actual "+value, "fail");
				//System.out.println("The expected attribute :"+attribute+" value does not contains the actual "+value);
			}
		} catch (WebDriverException e) {
			reportStep("Unknown exception occured while verifying the Attribute Text", "fail");
			//System.out.println("Unknown exception occured while verifying the Attribute Text");
		}
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			if(ele.isDisplayed()) {
				reportStep("The element "+ele+" is visible", "pass");
				//System.out.println("The element "+ele+" is visible");
			return true;
			} else {
				reportStep("The element "+ele+" is not visible", "fail");
				//System.out.println("The element "+ele+" is not visible");
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "fail");
			//System.out.println("WebDriverException : "+e.getMessage());
		} 
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			if(ele.isEnabled()) {
				reportStep("The element "+ele+" is Enabled", "pass");
				//System.out.println("The element "+ele+" is Enabled");
				return true;
			} else {
				reportStep("The element "+ele+" is not Enabled", "fail");
				//System.out.println("The element "+ele+" is not Enabled");
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "fail");
			//System.out.println("WebDriverException : "+e.getMessage());
		}
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			if(ele.isSelected()) {
				reportStep("The element "+ele+" is selected", "pass");
				//System.out.println("The element "+ele+" is selected");
				return true;
			} else {
				reportStep("The element "+ele+" is not selected", "fail");
				//System.out.println("The element "+ele+" is not selected");
			}
		} catch (WebDriverException e) {
			reportStep("WebDriverException : "+e.getMessage(), "fail");
			//System.out.println("WebDriverException : "+e.getMessage());
		}
		return false;
	}

	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}
	
	public String title() {
		return driver.getTitle();
	}
	
	public void startAppFB(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
				ChromeOptions op=new ChromeOptions();
				op.addArguments("-disable-notifications");
				driver = new ChromeDriver(op);
				reportStep("Chrome Browser launched", "pass");
		}
	}
	
	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
				reportStep("Chrome Browser launched", "pass");
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
				reportStep("FireFox Browser launched", "pass");
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				reportStep("IE Browser launched", "pass");
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			reportStep("The Browser Could not be Launched", "fail");
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		}finally {
			takeSnap();	
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
			reportStep("Locator selected successfully", "pass");
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator:"+locatorType+" Not Found with value: "+value, "fail");
			//System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			throw new RuntimeException();
		}finally {
			takeSnap();	
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		WebElement findElementById = driver.findElementById(value);
		return findElementById;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		try {
			switch(type.toLowerCase()) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "link": return driver.findElementsByLinkText(value);
			case "xpath": return driver.findElementsByXPath(value);
			}
			reportStep("Locator selected successfully", "pass");
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator:"+type+" Not Found with value: "+value, "fail");
			//System.err.println("The Element with locator:"+type+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert();
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.accept();
			System.out.println("The alert "+text+" is accepted.");
		} catch (NoAlertPresentException e) {
			System.out.println("There is no alert present.");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}  
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.dismiss();
			System.out.println("The alert "+text+" is accepted.");
		} catch (NoAlertPresentException e) {
			System.out.println("There is no alert present.");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}  
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
		} catch (NoAlertPresentException e) {
			System.out.println("There is no alert present.");
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		} 
		return text;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		driver.switchTo().alert().sendKeys(data);
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> allwind = driver.getWindowHandles();
			List<String> ls1= new ArrayList<>(allwind);
			String exwindow = ls1.get(index);
			driver.switchTo().window(exwindow);
			System.out.println("The Window With index: "+index+" switched successfully");
		} catch (NoSuchWindowException e) {
			System.err.println("The Window With index: "+index+" not found");	
		}finally {
		takeSnap();	
		}
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		try {
			Set<String> allWindows = driver.getWindowHandles();
			for (String eachWindow : allWindows) {
				driver.switchTo().window(eachWindow);
				if (driver.getTitle().equals(title)) {
					break;
				}
			}
			System.out.println("The Window With Title: "+title+
					"is switched ");
		} catch (NoSuchWindowException e) {
			System.err.println("The Window With Title: "+title+
					" not found");
		} finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(index);
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
		
	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(idOrName);
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		driver.switchTo().defaultContent();
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		if (driver.getCurrentUrl().equals(url)) {
			System.out.println("The url: "+url+" matched successfully");
		return true;
		} else {
			System.out.println("The url: "+url+" not matched");
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		if(title.equals(title)) {
			System.out.println("Page title: "+title+" matched successfully");
			return true;
		}
		else
		{
			System.out.println("Page url: "+title+" not matched");
			return false;
		}
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		try {
			File Src = driver.getScreenshotAs(OutputType.FILE);
			File Des = new File ("./Snaps/snap"+i+".png");
			FileUtils.copyFile(Src, Des);
			i++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Unable to print screenshots");
		}
	}
	
	@Override
	public void close() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		driver.quit();
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

}
